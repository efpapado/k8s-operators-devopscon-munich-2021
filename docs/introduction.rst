Introduction
============

Kubernetes is an open source platform, originally developed by Google,
to schedule workloads on a cluster of computers. The most basic unit
of work that kubernetes understands is a Pod, which is originally,
one or more docker containers with dedicated amount of compute resources.
These originally include CPU, RAM, network and disks.
This work of unit, is the source of viewing kubernetes as a
Container Scheduler. However, since the inception and birth of kubernetes,
work units have become more than just docker containers.
In the meanwhile Kubernetes, can schedule a lot of things. For example
`LXC containers`_, Full blown `virtual machines`_, `lightweight virtual machines`_,
and even `FreeBSD Jails`_.

As such, we should view Kubernetes as a Workload Scheduler or as a distributed
kernel, where we ask the system for compute resources, and as soon
as all requirements of the systems are solved, our request is fulfilled.

This kernel is composed of many controllers whose responsibility is fulfill a
desired state and react to changes in the actual state of what ever resources
they control. If you already work with kubernetes, you are probably
familiar with the built-in controllers ``Deployment`` and ``StatefulSet``

The developers of kubernetes recognised the need for adding custom user
controllers  already in early versions
of kubernetes. Version 1.7 add the ability to define  ``ThirdPartyResource``,
which allows extending kubernetes. These were later name ``CustomResourceDefinition``
in version 1.8 an onward.

Adding new resources to the kubernetes API opened the way for users writing
their own controllers which watch these resources and perform actions based
on changes in ``CustomResourceDefinition``.

For further reading, it is recommended reading the `Operator White Paper`_
published by the CNCF.

In this workshop, we will discuss some patterns in this paper,
and experiment with building our own operator using the Python and Go Language.

.. _LXC containers: https://github.com/automaticserver/lxe
.. _lightweight virtual machines: https://katacontainers.io/
.. _virtual machines: https://kubevirt.io/
.. _FreeBSD Jails: https://github.com/samuelkarp/runj
.. _Operator White Paper: https://github.com/cncf/tag-app-delivery/blob/main/operator-wg/whitepaper/CNCF_Operator_WhitePaper_v1-0_20210715.pdf

.. raw:: html

     <div style = "display:block; clear:both; page-break-after:always;"></div>
