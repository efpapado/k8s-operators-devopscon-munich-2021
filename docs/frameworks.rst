Frameworks for building Operators
=================================

Choosing a framework
--------------------
There are quite a few frameworks for building operators. These are mostly
written in Go and Python. But you will also find SKDs in ``.NET`` or ``Java``,
and even complete frameworks for writing operator code in Shell or Ansible

Here is an interesting statistic on the languages used to write operators:

.. figure:: _static/OperatorHub-Stats-1.jpg
   :scale: 33%
   :align: center

   Origin: https://hazelcast.com/blog/build-your-kubernetes-operator-with-the-right-tool/


obviously, since kubernetes itself is written in Go, the eco system of operators
it is also common in operators.

However, you don't have to choose Go. In fact, if you are not already fluent in Go, I would
advise to choose an operator framework which allows writing operators in Python.

Python allows you to write code quickly and prototype your operator logic quickly. In the
words of a smarter guy: `selecting a programming language can be a form of premature optimization. <https://snarky.ca/programming-language-selection-is-a-form-of-premature-optimization/>`_

Here are few notable operator frameworks:

 * `The Operator framework <https://sdk.operatorframework.io/>`_
 * `Kooper <https://github.com/spotahome/kooper>`_
 * `Kopf <https://kopf.readthedocs.io>`_
 * `Operator <https://github.com/canonical/operator>`_
 * `Shell Operator <https://github.com/flant/shell-operator>`_
 * `kubebuilder <https://book.kubebuilder.io/>`_

No framework or a mininal framework?
------------------------------------
 
In the words of another smart guy `Principles are more important than frameworks <https://sizovs.net/2018/12/17/stop-learning-frameworks/>`_.


`Frameworks don't make any sense <https://catonmat.net/frameworks-dont-make-sense>`_ hits a sensitive nerve
I have. However, there is a delicate balance found between a large over-complex framework and completly
writing everything from scratch.

In this workshop we will explore the go-client and Kopf. Both provide a simple API which is
easy to grasp quickly.

If you worked with Python's `Flask Microframework <https://github.com/pallets/flask/>`_,
you will feel at home with `kopf`.

The kubernetes `go-client,` allows you to define handler functions in go which response to cluster events.
The API of kopf is similar.

The Operator Framework is extremely large, and provides a CLI to create
automatically a project scaffold inlcuding kubernetes resources, webhooks, validations and even
Makefiles.
If you are a big fan of large frameworks, you should probably give it a try after you grasped
the basic principles of how to write an operator.
