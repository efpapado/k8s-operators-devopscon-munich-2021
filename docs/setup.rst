Setup minikube
==============

To practice building operators you should have access to a kubernetes cluster.
Ideally, one where you have admin right, and you are not afraid of wrecking havoc.

`minikube`_ offers a way to quickly setup a kubernetes cluster on your developer machine.

Follow the installation guide to get minikube installed. For starting a cluster on a Windows
machines you need either Docker or VirtualBox installed to.
I will be using Gentoo and the KVM backend for the demo, however all commands should
work on your OS too.

Starting a kubernetes cluster
-----------------------------

In your terminal shell do::

        $ minikube config set memory 2848
        $ minikube config set cpus 2
        $ minikube config set vm-driver kvm22
        $ minikube start -n 2 -p devopscon

You should see something similar::

        oznt@yeni2:~ $ minikube start -p devopscon
        😄  [devopscon] minikube v1.24.0 on Gentoo 2.7
        ✨  Using the kvm2 driver based on user configuration
        👍  Starting control plane node devopscon in cluster devopscon
        🔥  Creating kvm2 VM (CPUs=2, Memory=2848MB, Disk=20000MB) ...
        🐳  Preparing Kubernetes v1.22.3 on Docker 20.10.8 ...
            ▪ Generating certificates and keys ...
            ▪ Booting up control plane ...
            ▪ Configuring RBAC rules ...
        🔎  Verifying Kubernetes components...
            ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
        🌟  Enabled addons: default-storageclass, storage-provisioner
        🏄  Done! kubectl is now configured to use "devopscon" cluster and "default" namespace by default


You can add more nodes to the cluster with::


        $ oznt@yeni2:~ $ minikube node add -p devopscon
        😄  Adding node m02 to cluster devopscon
        ❗  Cluster was created without any CNI, adding a node to it might cause broken networking.
        👍  Starting worker node devopscon-m02 in cluster devopscon
        🔥  Creating kvm2 VM (CPUs=2, Memory=2848MB, Disk=20000MB) ...
        🐳  Preparing Kubernetes v1.22.3 on Docker 20.10.8 ...
        🔎  Verifying Kubernetes components...
        🏄  Successfully added m02 to devopscon!

.. raw:: html

   <div style = "display:block; clear:both; page-break-after:always;"></div>

You can now view all the nodes in your cluster::

        $ kubectl get nodes
        NAME            STATUS   ROLES                  AGE   VERSION
        devopscon       Ready    control-plane,master   55m   v1.22.3
        devopscon-m02   Ready    <none>                 27m   v1.22.3

and check that your cluster is ready to start working with::

        oznt@yeni2:~ $ kubectl get pods -A
        NAMESPACE     NAME                                READY   STATUS    RESTARTS      AGE
        kube-system   coredns-78fcd69978-bbm9f            1/1     Running   0             55m
        kube-system   etcd-devopscon                      1/1     Running   0             55m
        kube-system   kindnet-56m6l                       1/1     Running   0             27m
        kube-system   kindnet-svkhm                       1/1     Running   0             27m
        kube-system   kube-apiserver-devopscon            1/1     Running   0             55m
        kube-system   kube-controller-manager-devopscon   1/1     Running   0             55m
        kube-system   kube-proxy-mjksn                    1/1     Running   0             28m
        kube-system   kube-proxy-p7x5m                    1/1     Running   0             55m
        kube-system   kube-scheduler-devopscon            1/1     Running   0             55m
        kube-system   storage-provisioner                 1/1     Running   1 (54m ago)   55m


.. raw:: html

   <div style="display:block; clear:both; page-break-after:always;"></div>

.. _minikube: https://minikube.sigs.k8s.io/
