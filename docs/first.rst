Your first Operator
===================

We begin our journey for working with Operators with a simpler operators
written in BASH. This is a simple Operator that does only one thing: Counting
the Deployments and Pods in the cluster.

We start the operator and watch what Grafzahl [1]_ says::

        $ bash operators/00-grafzahl/main.sh
        Deployments now 1, previously 0
        Pods now 10, previously 0
        Pods number increased! 🧛 is 😄!
        Deployments number increased! 🧛 is 😄!
        Deployments now 1, previously 1


In a different terminal we start a Pod::

        $ kubectl run nginx --image docker.io/nginx
        pod/nginx created

Now Grafzahl says::

        Deployments now 1, previously 1
        Pods now 11, previously 10
        Pods number increased! 🧛 is 😄!
        Deployments number is the same! 🧛 is 😐!

We delete the Pod::

        $ kubectl delete pod nginx
        pod "nginx" deleted

Now Grafzahl is sad::

        Deployments now 1, previously 1
        Pods now 10, previously 11
        Pods number is the smaller! 🧛 is 😔
        Deployments number is the same! 🧛 is 😐!

 
So, this is a very simple operator. It has tons of problem. First,
despite the ubiquity of BASH, most programmers are not fluent in it,
and large BASH programs tend to become one big function which is hard
to modify. In addition, BASH does not have built in abilities to work
with JSON, and learing `jq`_ might not be your highest priority now.

.. [1] Graf Zahl is the German name for the Sesame Street character Count von Count.

.. _jq: https://stedolan.github.io/jq/

|pagebreak|

The 2 most obvious problem of Grafzahl, is the it's not running in the
cluster, and that it does not have any state. If this "operator" is
restarted it loses all it's information.

We begin by fixing the first problem, by packaging Grafzahl in a Container.
Being lazy, I like wrapping docker commands with a ``Makefile``, so here
is the ``Dockerfile``::


        $ cat Dockerfile
        FROM docker.io/alpine:latest
        
        ADD main.sh /usr/local/bin/grafzahl
        
        RUN chmod +x /usr/local/bin/grafzahl
        
        ENTRYPOINT /usr/local/bin/grafzahl

And the ``Makefile``::

        $ cat Makefile
        
        docker-build:
                docker build -t $(REGISTRY)/$(ORG)/grafzahl:$(VERSION) .
        
        
        docker-push:
                docker push $(REGISTRY)/$(ORG)/grafzahl:$(VERSION)
        
        
Now, let's build the image and push it in one step::

        $ make docker-build docker-push ORG=oz123 REGISTRY=docker.io VERSION=0.0.1
        docker build -t docker.io/oz123/grafzahl:0.0.1 .
        Sending build context to Docker daemon   5.12kB
        Step 1/4 : FROM docker.io/alpine:latest
         ---> c059bfaa849c
        Step 2/4 : ADD main.sh /usr/local/bin/grafzahl
         ---> Using cache
         ---> 019a5866fe79
        Step 3/4 : RUN chmod +x /usr/local/bin/grafzahl
         ---> Using cache
         ---> 28d66491ab1f
        Step 4/4 : ENTRYPOINT /usr/local/bin/grafzahl
         ---> Using cache
         ---> dc583dd928cb
        Successfully built dc583dd928cb
        Successfully tagged oz123/grafzahl:0.0.1
        docker push docker.io/oz123/grafzahl:0.0.1
        The push refers to repository [docker.io/oz123/grafzahl]
        cd79aba87220: Pushed
        0deda38dbf9c: Pushed
        8d3ac3489996: Mounted from library/alpine
        0.0.1: digest: sha256:9001706fa5724ad0ad177b9cef1b1856e9d24110e22727b74b157496a8cab8c3 size: 942


Running the Operator inside the cluster
---------------------------------------

The operator is ready to run in the cluster. It's a good practice to run different projects
in different namespace. Hence, before we run the operator, we create a namespace for it::

        $ kubectl create namespace grafzahl
        namespace/grafzahl created

Now, let's try and run the opertor in kubernetes::

        kubectl -n grafzahl run grafzahl --image oz123/grafzahl:0.0.1
        pod/grafzahl created


If we follow the logs of the container we will see an error::

        $ kubectl logs -n grafzahl grafzahl
        Error from server (Forbidden): deployments.apps is forbidden: User "system:serviceaccount:grafzahl:default" cannot list resource "deployments" in API group "apps" at the cluster scope
        Error from server (Forbidden): pods is forbidden: User "system:serviceaccount:grafzahl:default" cannot list resource "pods" in API group "" at the cluster scope
        Deployments now 0, previously 0
        Pods now 0, previously 0
        Pods number is the same! 🧛 is 😐!
        Deployments number is the same! 🧛 is 😐!


To fix this, we have to excurse to how Pods are started in kubernetes.


Namespaces, ServiceAccounts, and RBAC 
-------------------------------------

That's a big title up there. But it is essential for us to explore those subjects
before we can continue with operators.

We start with examining the error::

      User "system:serviceaccount:grafzahl:default" cannot list resource "deployments" in API group "apps" at the cluster scope

We see that there is a user invovled here. What?

When we first created the namespace ``grafzahl`` something happened behind the scenes.
kubernetes creates a default serivce account for the namespace, and this service account is
assigned to all pods if not specified otherwise.

To communicate with the kubernetes API server you need to authenticate yourself, and tell kubernetes
who you are, before you tell the API what you want. To give you what you want, the API
server needs to decide if you are allowed to ask for this resource.

There are mulitple ways to identify yourself. ``ServiceAccounts`` use a certificate to do it.

When you used ``kubectl`` to communicate with ``minikube`` you used a special super-user certificate::

     $ cert=$(kubectl config view -o jsonpath='{.users[?(@.name == "devopscon")].user.client-certificate}') 
     $ openssl x509 -in ${cert} -noout -text | head -n 20
     Certificate:
        Data:
           Version: 3 (0x2)
           Serial Number: 2 (0x2)
           Signature Algorithm: sha256WithRSAEncryption
           Issuer: CN = minikubeCA
           Validity
               Not Before: Nov 25 14:15:26 2021 GMT
               Not After : Nov 25 14:15:26 2024 GMT
           Subject: O = system:masters, CN = minikube-user
           Subject Public Key Info:
               Public Key Algorithm: rsaEncryption
                   RSA Public-Key: (2048 bit)
                   Modulus:
                       00:f0:51:ed:34:30:aa:b1:5b:1c:81:1b:92:b1:c0:
                       fa:26:05:1d:8b:a7:27:82:33:da:54:7a:34:98:0e:
                       95:03:92:c8:3c:97:78:37:99:24:13:b1:b9:0f:56:
                       9c:2d:72:05:38:98:ab:df:87:b5:47:a5:45:3d:0e:
                       1a:e1:a8:02:b1:e0:2b:36:3a:18:29:a2:35:08:38:
                       78:b6:99:3c:fd:7a:5d:72:e7:4f:58:92:6b:b7:38:



As you can see, this certificate contains infomation that the user ``minikube-user``
belongs to the group (Orgnanizational Unit) ``system:masters``.

But what about the user in the ``grafzahl`` Pod? Well, to be exact this
is not really a user, but rather a ``ServiceAccout`` which is auomatically assigned
because we didn't specify any::

    $ kubectl get pod -n grafzahl -o yaml
    apiVersion: v1
    items:
    - apiVersion: v1
      kind: Pod
      metadata:
        name: grafzahl
        namespace: grafzahl
        ...
      spec:
        containers:
        - image: oz123/grafzahl:0.0.1
          imagePullPolicy: IfNotPresent
          name: grafzahl
          ...
          volumeMounts:
          - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
            name: kube-api-access-k7j92
            readOnly: true
        ...
        serviceAccount: default
        serviceAccountName: default
        ...
        volumes:
        - name: kube-api-access-k7j92
          projected:
            defaultMode: 420
            sources:
            - serviceAccountToken:
                expirationSeconds: 3607
                path: token
            - configMap:
                items:
                - key: ca.crt
                  path: ca.crt
                name: kube-root-ca.crt
    

kubernetes adds to the rather terse ``Pod`` yaml we created a bunch
of other things. Along with the ``default`` ``ServiceAccout`` there are also
``Volumes`` and ``configMap`` added.

Let's examine what kubenetes created when we created the ``Namespace`` ``grafzahl``::

    $ k get namespaces grafzahl -o yaml
    apiVersion: v1
    kind: Namespace
    metadata:
      creationTimestamp: "2021-11-26T19:36:26Z"
      labels:
        kubernetes.io/metadata.name: grafzahl
      name: grafzahl
      resourceVersion: "15774"
      uid: 4b4ffc98-341d-4f02-a416-b7cba2d16df7
    spec:
      finalizers:
      - kubernetes
    status:
      phase: Active

Immidietly after the ``Namespace`` was created, a ``ServiceAccount``, a ``Secret``
associated with it, and a ``ConfigMap`` were created::


    $ k get serviceaccounts -n grafzahl -o yaml
    apiVersion: v1
    items:
    - apiVersion: v1
      kind: ServiceAccount
      metadata:
        creationTimestamp: "2021-11-26T19:36:26Z"
        name: default
        namespace: grafzahl
        resourceVersion: "15778"
        uid: b10148cc-0a6a-442f-997b-56a7f434c6d6
      secrets:
      - name: default-token-7z5bh
    kind: List
    metadata:
      resourceVersion: ""
      selfLink: ""

    $ k get secrets -n grafzahl -o yaml
    apiVersion: v1
    items:
    - apiVersion: v1
      data:
        ca.crt: LS0tLS1CRUdJTiBDRVJUSUZJQ0....==
        namespace: Z3JhZnphaGw=
        token: ZXlKaGJHY2lPaUpTVX3....ppUXc=
      kind: Secret
      metadata:
        annotations:
          kubernetes.io/service-account.name: default
    
   $ k get configmaps -n grafzahl -o yaml
   apiVersion: v1
   items:
   - apiVersion: v1
     data:
       ca.crt: |
         -----BEGIN CERTIFICATE-----
         MIIDBj...
         ....JuGKK+cZuhwK7sFU2Zs7HdY20XGpbr
         Oh4n6QpriUx6ug==
         -----END CERTIFICATE-----
     kind: ConfigMap
     metadata:
       annotations:
         kubernetes.io/description: Contains a CA bundle that can be used to verify the
           kube-apiserver when using internal endpoints such as the internal service
           IP or kubernetes.default.svc. No other usage is guaranteed across distributions
           of Kubernetes clusters.
       creationTimestamp: "2021-11-26T19:36:26Z"
       name: kube-root-ca.crt
       namespace: grafzahl
       resourceVersion: "15775"
       uid: 0bc75155-20e2-442e-bfe4-6cf00d4db308
   kind: List
   metadata:
     resourceVersion: ""
     selfLink: ""

Let's examine this inside the container::

        $ k exec -it -n grafzahl grafzahl -- /bin/bash
        bash-5.1# kubectl config view
        apiVersion: v1
        clusters: null
        contexts: null
        current-context: ""
        kind: Config
        preferences: {}
        users: null
        bash-5.1#

It might be surprising, but there is no ``~/kube/.config`` file. So how does
``kubectl`` knows how to communicate with the API server?

Well, all kubernetes client libraries (in this case,
the GoLang one, in which kubectl is written) look for this information in two
places. First, the look at the process's environment for API address::

        bash-5.1# env
        KUBERNETES_SERVICE_PORT_HTTPS=443
        KUBERNETES_SERVICE_PORT=443
        HOSTNAME=grafzahl
        PWD=/
        HOME=/root
        KUBERNETES_PORT_443_TCP=tcp://10.96.0.1:443
        TERM=xterm
        SHLVL=1
        KUBERNETES_PORT_443_TCP_PROTO=tcp
        KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
        KUBERNETES_SERVICE_HOST=10.96.0.1
        KUBERNETES_PORT=tcp://10.96.0.1:443
        KUBERNETES_PORT_443_TCP_PORT=443
        PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
        _=/usr/bin/env


We can check that this works with::

        bash-5.1# curl -k https://$KUBERNETES_SERVICE_HOST
        {
          "kind": "Status",
          "apiVersion": "v1",
          "metadata": {
        
          },
          "status": "Failure",
          "message": "forbidden: User \"system:anonymous\" cannot get path \"/\"",
          "reason": "Forbidden",
          "details": {
        
          },
          "code": 403

As you can see we didn't tell the API server who we are.

Second, the client libraries look for the ``ServiceAccount`` information in::

        /run/secrets/kubernetes.io/serviceaccount

Look again at all the things mounted into this path in the ``Pod`` yaml output above.
This are all the things needed to the the kubernetes API who we are.

We can use the token in ``/run/secret/kubernetes.io/serviceaccount`` to identify ourselves::

        bash-5.1# token=$(cat /run/secrets/kubernetes.io/serviceaccount/token)
        bash-5.1# curl -k  -X GET  -H "Authorization: Bearer ${token}" 'https://10.96.0.1:443/'
        {
        "kind": "Status",
        "apiVersion": "v1",
        "metadata": {
      
        },
        "status": "Failure",
        "message": "forbidden: User \"system:serviceaccount:grafzahl:default\" cannot get path \"/\"",
        "reason": "Forbidden",
        "details": {
      
        },
        "code": 403
        }
      
Well, good news, the API servers no longer thinks we are ``anonymous``. But what can we do with this token?
We can list all the resources in the cluster with::

       $ curl -k  -X GET  -H "Authorization: Bearer ${token}" 'https://10.96.0.1:443/api/v1/'
       bash-5.1#curl -k  -X GET  -H "Authorization: Bearer ${token}" 'https://10.96.0.1:443/api/v1/'
       {
        "kind": "APIResourceList",
        "groupVersion": "v1",
        "resources": [
          {
            "name": "bindings",
            "singularName": "",
            "namespaced": true,
            "kind": "Binding",
            "verbs": [
              "create"
            ]
          },
          {
            "name": "componentstatuses",
            "singularName": "",
            "namespaced": false,
            "kind": "ComponentStatus",
            "verbs": [
              "get",
              "list"
            ],
            "shortNames": [
              "cs"
            ]
          },
          {
            "name": "configmaps",
            "singularName": "",
            "namespaced": true,
            "kind": "ConfigMap",
            "verbs": [
              "create",
              "delete",
              "deletecollection",
              "get",
              "list",
              "patch",
              "update",
              "watch"
            ],
            "shortNames": [
              "cm"
            ],
       ...
       }


We can try and list some of the resources::

        bash-5.1# curl -k  -X GET  -H "Authorization: Bearer ${token}" 'https://10.96.0.1:443/api/v1/cm'
        {
        "kind": "Status",
        "apiVersion": "v1",
         "metadata": {

        },
        "status": "Failure",
        "message": "cm is forbidden: User \"system:serviceaccount:grafzahl:default\" cannot list resource \"cm\" in API group \"\" at the cluster scope",
        "reason": "Forbidden",
        "details": {
        "kind": "cm"
        },
        "code": 403

This is not really erogonomic, and ``kubectl`` has a much nicer way of doing this::

        bash-5.1# kubectl auth can-i get pods
        no
        bash-5.1# kubectl auth can-i get cm
        no

|pagebreak|

We can install a `access-matrix <https://github.com/corneliusweig/rakkess>`_
plugin for kubectl which is even nicer::


   bash-5.1# kubectl access-matrix
   NAME                                                          LIST  CREATE  UPDATE  DELETE
   apiservices.apiregistration.k8s.io                            ✖     ✖       ✖       ✖

   bindings                                                            ✖
   certificatesigningrequests.certificates.k8s.io                ✖     ✖       ✖       ✖
   clusterrolebindings.rbac.authorization.k8s.io                 ✖     ✖       ✖       ✖
   clusterroles.rbac.authorization.k8s.io                        ✖     ✖       ✖       ✖
   componentstatuses                                             ✖
   configmaps                                                    ✖     ✖       ✖       ✖
   controllerrevisions.apps                                      ✖     ✖       ✖       ✖
   cronjobs.batch                                                ✖     ✖       ✖       ✖
   csidrivers.storage.k8s.io                                     ✖     ✖       ✖       ✖
   csinodes.storage.k8s.io                                       ✖     ✖       ✖       ✖
   csistoragecapacities.storage.k8s.io                           ✖     ✖       ✖       ✖
   customresourcedefinitions.apiextensions.k8s.io                ✖     ✖       ✖       ✖
   daemonsets.apps                                               ✖     ✖       ✖       ✖
   deployments.apps                                              ✖     ✖       ✖       ✖
   endpoints                                                     ✖     ✖       ✖       ✖
   endpointslices.discovery.k8s.io                               ✖     ✖       ✖       ✖
   events                                                        ✖     ✖       ✖       ✖
   events.events.k8s.io                                          ✖     ✖       ✖       ✖
   flowschemas.flowcontrol.apiserver.k8s.io                      ✖     ✖       ✖       ✖
   horizontalpodautoscalers.autoscaling                          ✖     ✖       ✖       ✖
   ingressclasses.networking.k8s.io                              ✖     ✖       ✖       ✖
   ingresses.networking.k8s.io                                   ✖     ✖       ✖       ✖
   runtimeclasses.node.k8s.io                                    ✖     ✖       ✖       ✖
   secrets                                                       ✖     ✖       ✖       ✖
   selfsubjectaccessreviews.authorization.k8s.io                       ✔
   selfsubjectrulesreviews.authorization.k8s.io                        ✔
   serviceaccounts                                               ✖     ✖       ✖       ✖
   services                                                      ✖     ✖       ✖       ✖
   statefulsets.apps                                             ✖     ✖       ✖       ✖
   storageclasses.storage.k8s.io                                 ✖     ✖       ✖       ✖
   ...

.. tip::

   Any program found in your path and starting with kubectl can be used as a plugin.
            
You can install access-matrix with::
        
   bash-5.1# wget https://github.com/corneliusweig/rakkess/releases/download/v0.5.0/access-matrix-amd64-linux.tar.gz
   bash-5.1# tar xvzf access-matrix-amd64-linux.tar.gz
   bash-5.1# mv access-matrix-amd64-linux /usr/local/bin/kubectl-access_matrix


OK, so there is not much we can do while authenticating as `default` ServiceAccount in the `grafzahl` namespace.

To fix this we need to create a ``Role`` with the correct permissions, and then associate this ``Role``
with a service account via ``RoleBinding``.

|pagebreak|

Granting Grafzahl permissions
-----------------------------

When we created the namespace a service account was created for us.
Grafzahl is an opertor that needs permissions to watch Pods and Deployments
in the whole cluster. Hence, it needs a ``Role`` which globally available.
Hence, we will define a ``ClusterRole`` and repectively use a
``ClusterRoleBindings``:

.. code:: yaml

   apiVersion: rbac.authorization.k8s.io/v1
   kind: ClusterRole
   metadata:
     name: grafzahl-rc
   rules:
   - apiGroups: ["apps"]
     resources: ["deployments"]
     verbs: ["get", "list"]
   - apiGroups: [""]
     resources: ["pods"]
     verbs: ["get", "list"]
   ---
   apiVersion: rbac.authorization.k8s.io/v1
   kind: ClusterRoleBinding
   metadata:
     name: grafazahl
   roleRef:
     apiGroup: rbac.authorization.k8s.io
     kind: ClusterRole
     name: grafzahl-rc
   subjects:
   - apiGroup: rbac.authorization.k8s.io
     kind: User
     name: system:serviceaccount:grafzahl:default


.. note::

        It is possible that you will have namespaced operators, which will then require
        namespaced ``Role`` and ``RoleBinding`` definitions.


We apply the manifest with::

        $ k apply -f clusterrole.yml
        clusterrole.rbac.authorization.k8s.io/grafzahl-rc created
        clusterrolebinding.rbac.authorization.k8s.io/grafazahl created

We can now check that the permissions work with::

        $ k exec -n grafzahl grafzahl -it -- kubectl auth can-i get deployments
        yes
        $ k exec -n grafzahl grafzahl -it -- kubectl auth can-i get pods
        yes
 


We can also check this with::

        $ k access-matrix --as system:serviceaccount:grafzahl:default | grep -e pods -e deploy
        deployments.apps                                              ✔     ✖       ✖       ✖
        pods                                                          ✔     ✖       ✖       ✖
        podsecuritypolicies.policy                                    ✖     ✖       ✖       ✖

With this we got Grafzahl to work. But there is still on basic problem with this operator.
It lacks state management. If kubernetes decides to kill the pods due to resource presure or
node evacuation, all the counting will be lost, and grafzahl will need to count the pods again.

State management with CustomResourceDefinitions and lables
----------------------------------------------------------

As mentioned earlier, we can extend the kubernetes API by defining our own resources.
For Grafzahl, we will begin with a simple CRD which saves the total number of Pods and Deployments:


.. code:: yaml

   apiVersion: apiextensions.k8s.io/v1
   kind: CustomResourceDefinition
   metadata:
     name: numbers.grafzahl.io
   spec:
     group: grafzahl.io
     scope: Cluster
     names:
       plural: numbers
       singular: number
       kind: GrafzahlNumber
       shortNames:
       - gn
     versions:
     - name: v1beta1
       served: true
       storage: true
       schema:
         openAPIV3Schema:
           type: object
           properties:
             spec:
               type: object
               properties:
                 deployments:
                   type: integer
                 pods:
                   type: integer
       additionalPrinterColumns:
       - name: Deployments
         type: integer
         description: Total number of deployments
         jsonPath: .spec.deployments
       - name: Pods
         type: integer
         jsonPath: .spec.pods
   
Applying this manifest to the cluster::

        $ k apply -f crd.yml
        customresourcedefinition.apiextensions.k8s.io/numbers.grafzahl.io created

Now, we test that our ``CustomResourceDefinition`` is working with:

.. code:: yaml

   # numbers.yml 
   apiVersion: "grafzahl.io/v1beta1"
   kind: GrafzahlNumber
   metadata:
     name: total
   spec:
     pods: 2
     deployments: 3

Creating a ``numbers`` resource::

    $ k apply -f numbers.yml
    grafzahlnumber.grafzahl.io/total created

And checking that this works::

    $ k get gn
    NAME    DEPLOYMENTS   PODS
    total   3             2

    $ k get numbers.grafzahl.io
    NAME    DEPLOYMENTS   PODS
    total   3             2


So, now we can use this ``CustomResourceDefinition`` to keep track on the total
numbers even if the Grafzahl pod is restarted. Let's change the code to do this::

    #!/bin/bash
    ###
    # Grafzahl likes counting things, and when there are more things to counts the happier
    # he is.
    # This silly operator just counts Deployments and Pods in the cluster.
    ###
    
    # tell bash to crash if any error is encountered
    set -e 
    
    NUM_PODS=$(kubectl get numbers.grafzahl.io total -o jsonpath='{.spec.pods}')
    NUM_DEPLOYMENTS=$(kubectl get numbers.grafzahl.io total -o jsonpath='{.spec.deployments}')
    
    while true
    do
    	DEPLOYMENTS=$(kubectl get deployments.apps -A -o json | jq '.items | length')
    	PODS=$(kubectl get pods -A -o json | jq '.items | length')
    	PATCH_P=true
    	PATCH_D=true
    	echo "Deployments now ${DEPLOYMENTS}, previously ${NUM_DEPLOYMENTS}"
    	echo "Pods now ${PODS}, previously  ${NUM_PODS}"
    	if [[ $PODS -gt $NUM_PODS ]]; then
    		echo "Pods number increased! 🧛 is 😄!"
    	elif [[  $PODS -lt $NUM_PODS ]]; then
    		echo "Pods number is the smaller! 🧛 is 😔"
    	elif [[ $PODS -eq $NUM_PODS ]]; then
    		echo "Pods number is the same! 🧛 is 😐!"
    		PATCH_P=false
    	fi
    
    	if [[ $DEPLOYMENTS -gt $NUM_DEPLOYMENTS ]]; then
    		echo "Deployments number increased! 🧛 is 😄!"
    	elif [[  $DEPLOYMENTS -lt $NUM_DEPLOYMENTS ]]; then
    		echo "Deployments number is the smaller! 🧛 is !😔"
    	elif [[ $DEPLOYMENTS -eq $NUM_DEPLOYMENTS ]]; then
    		echo "Deployments number is the same! 🧛 is 😐!"
    		PATCH_D=false
    	fi
    	
    	if [ ${PATCH_P} == true ]; then
    		kubectl patch numbers.grafzahl.io total --type merge -p "{\"spec\": {\"pods\": ${PODS}}}"
    	fi
    	
    	if [ ${PATCH_D} == true ]; then
    		kubectl patch numbers.grafzahl.io total --type merge -p "{\"spec\": {\"pods\": ${DEPLOYMENTS}}}"
    	fi
    	
    	NUM_PODS=${PODS}
    	NUM_DEPLOYMENTS=${DEPLOYMENTS}
    
    	sleep 10
    done


I already built a new image containing this modified code, so we can just set the pod image to use
the updated image:

.. code:: yaml

   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: grafzahl-operator
     namespace: grafzahl
   spec:
     replicas: 1
     selector:
       matchLabels:
         app: grafzahl
     template:
       metadata:
         labels:
           app: grafzahl
       spec:
         containers:
         - name: main
           image: docker.io/oz123/grafzahl:0.0.2


Applying it with::

    $ k apply -f deployment.yml

The container will fail! This again because, it lacks permissions to read and modify
the newly created ``CustomResourceDefinition``::

   $ k logs -n grafzahl grafzahl-operator-779c494db6-nvkq8
   Error from server (Forbidden): numbers.grafzahl.io "total" is forbidden: User "system:serviceaccount:grafzahl:default" cannot get resource "numbers" in API group "grafzahl.io" at the cluster scope
   
Let's fix it by applying a modified version of the ``ClusterRole``::

   $ cat <<EOF | kubectl apply -f -
   apiVersion: rbac.authorization.k8s.io/v1
   kind: ClusterRole
   metadata:
     name: grafzahl-rc
   rules:
   - apiGroups: ["apps"]
     resources: ["deployments"]
     verbs: ["get", "list"]
   - apiGroups: [""]
     resources: ["pods"]
     verbs: ["get", "list"]
   - apiGroups: ["grafzahl.io"]
     resources: ["numbers"]
     verbs: ["get", "list", "patch"]
   ---
   apiVersion: rbac.authorization.k8s.io/v1
   kind: ClusterRoleBinding
   metadata:
     name: grafazahl
   roleRef:
     apiGroup: rbac.authorization.k8s.io
     kind: ClusterRole
     name: grafzahl-rc
   subjects:
   - apiGroup: rbac.authorization.k8s.io
     kind: User
     name: system:serviceaccount:grafzahl:default
   EOF

And now ``Grafzahl`` will work::

   $ k logs -n grafzahl grafzahl-operator-779c494db6-mwgvs main -f
   Deployments now 2, previously 0
   Pods now 11, previously  1
   Pods number increased! 🧛 is 😄!
   Deployments number increased! 🧛 is 😄!
   grafzahlnumber.grafzahl.io/total patched
   grafzahlnumber.grafzahl.io/total patched
   Deployments now 2, previously 2
   Pods now 11, previously  11
   Pods number is the same! 🧛 is 😐!
   Deployments number is the same! 🧛 is 😐!


Now, ``Grafzahl`` has it's counters saved in ``etcd``. However, if we restart the pod, we will
still count again elements which were already counted. This might not change the overall accuracy
of grafzahl, but the act of counting what is already counted is redundant.
We can improve this adding a lablel to each counted ``Pod`` or ``Deployment``, so grafzahl can
ignore counting them again.

To do this we can use the following filter::

   $ k get pods -A -l grafzahl!=counted

To prove this works::

   $ k run --image=docker.io/nginx nginx -l grafzahl=counted
   $ k get pod  -A -l grafzahl!=counted
   NAMESPACE     NAME                                 READY   STATUS    RESTARTS        AGE
   grafzahl      grafzahl-operator-779c494db6-mwgvs   1/1     Running   0               99m
   kube-system   coredns-78fcd69978-xrvgf             1/1     Running   0               16h
   kube-system   etcd-devopscon                       1/1     Running   1 (2d13h ago)   2d22h
   kube-system   kindnet-56m6l                        1/1     Running   3 (4h11m ago)   2d22h
   kube-system   kindnet-svkhm                        1/1     Running   3 (4h11m ago)   2d22h
   kube-system   kube-apiserver-devopscon             1/1     Running   1 (2d13h ago)   2d22h
   kube-system   kube-controller-manager-devopscon    1/1     Running   1 (2d13h ago)   2d22h
   kube-system   kube-proxy-mjksn                     1/1     Running   1 (2d13h ago)   2d22h
   kube-system   kube-proxy-p7x5m                     1/1     Running   1 (2d13h ago)   2d22h
   kube-system   kube-scheduler-devopscon             1/1     Running   1 (2d13h ago)   2d22h
   kube-system   storage-provisioner                  1/1     Running   8 (4h11m ago)   2d22h


As we can see ``nginx`` is not included. Once a new Pod or Deployment our found we can use the following
to add the correct label::

        $ kubectl patch pod nginx -n default --type merge -p '{"metadata": {"labels": {"grafzahl": "counted"}}}'

The previously show operator code was beginning to look like spaghetti, with a bit of refactoring
we can reuse a lot of code::

   #!/bin/bash
   
   NUM_PODS=$(kubectl get numbers.grafzahl.io total -o jsonpath='{.spec.pods}')
   NUM_DEPLOYMENTS=$(kubectl get numbers.grafzahl.io total -o jsonpath='{.spec.deployments}')
   
   function check() {
   	local resource=$1
   
   	case $resource in
   	
   	deployment)
   		PREVIOUS_NUM=$NUM_DEPLOYMENTS
   		;;
   	pod)
   		PREVIOUS_NUM=$NUM_PODS
   		;;
   	*)
       		echo -n "Unknown resource";
   		exit 1;
   		;;
   
   	esac
   	# create an assosiative array with all deployments names and namespaces
   	declare -A NEW=()
   	while read -r a b; do
   		NEW["$a"]="$b"
   	done < <(kubectl get $resource -l grafzahl!=counted -A -o=jsonpath="{range .items[*]}{.metadata.name}{' '}{.metadata.namespace}{'\n'}{end}")
   
   	if [[ ${#NEW[@]} -gt 0 ]]; then
   		echo "Found ${#NEW[@]} new ${resource^}s, ${resource} number increased! 🧛 is 😄! (previous: $PREVIOUS_NUM)"
   
   		for key in "${!NEW[@]}"; do
   			kubectl patch ${resource} "$key" -n ${NEW["$key"]} --type merge -p '{"metadata": {"labels": {"grafzahl": "counted"}}}'
   		done
   
   		NEW_TOTAL=$(( ${#NEW[@]} + PREVIOUS_NUM ))
   		kubectl patch numbers.grafzahl.io total --type json  -p="[{\"op\": \"replace\", \"path\": \"/spec/${resource}s\", \"value\": ${NEW_TOTAL}}]"
   		return $NEW_TOTAL
   
   	fi
   
   	ALL=$(kubectl get ${resource} -A -o json | jq '.items | length')
   
   
   	if [[  ${ALL} -lt ${PREVIOUS_NUM} ]]; then
   		echo "${resource^} number is the smaller! 🧛 is 😔  (previous: $PREVIOUS_NUM)"
   		kubectl patch numbers.grafzahl.io total --type json  -p="[{\"op\": \"replace\", \"path\": \"/spec/${resource}s\", \"value\": ${ALL}}]"
   	elif [[ ${ALL} -eq ${PREVIOUS_NUM} ]]; then
   		echo "${resource^} number is the same! 🧛 is 😐!"
   	fi
   
   	return $ALL
   }
   
   while true
   do
   	check pod
   	NUM_PODS=$?
   	check deployment
   	NUM_DEPLOYMENTS=$?
   	sleep 5
   done

We already solved a lot of problems with our grafzahl operator. However, the operator is still
no re-entrant. If we start the operator with all objects already lables we will see strange counting.
To fix this, we add a little change::

        ...
        function onStart(){
	local markedPods=$(kubectl get pods -l grafzahl=counted -A -o json | jq '.items | length')
	local markedDeployments=$(kubectl get deployments -l grafzahl=counted -A -o json | jq '.items | length')

	if [ $NUM_PODS -ne $markedPods ]; then
		kubectl patch numbers.grafzahl.io total --type json  -p="[{\"op\": \"replace\", \"path\": \"/spec/pods\", \"value\": ${markedPods}}]"
		echo "Updated total Pods: ${markedPods}"
	fi

	if [ $NUM_DEPLOYMENTS -ne $markedDeployments ]; then
		kubectl patch numbers.grafzahl.io total --type json  -p="[{\"op\": \"replace\", \"path\": \"/spec/deployments\", \"value\": ${markedDeployments}}]"
		echo "Updated total Deployments: ${markedDeployments}"
	fi

        }

        ...
        while true
        do
                check pod
                NUM_PODS=$?
   	        check deployment
   	        NUM_DEPLOYMENTS=$?
   	        sleep 5
        done

So far, it seems this operator does a fine job of tracking the total number of pods and deployments.
However, on a large enough cluster, it seems that polling the number of unlabled Pods or Deployments is ...
Well, to put it mildly not well scaling. The is a much better option, we can use ``Events``.

Events for Operators
--------------------

Instead of actively asking kubernetes for new resources and tracking changes ourselves,
we can user ``Events``, by doing so we are going to simplify our labour. Check out the
events that happend in the cluster in the last hour::

        $ kubectl get events -A

.. tip:: 

   Per default events are only saved for 1 hour in the cluster.
   You can changes this by passing a command line option to kube-api-server:
   ``--event-ttl duration``


Subscribing to ``Events`` can be done very elegantly with asynchronous code. And
while possible, it is probably not easy doing with BASH.

Danke Grafzahl
--------------

We created a small operator in BASH. And while, I admit it's a weired choice, it allowed
us to concentrate on the principles of what needs to be done to create an operator,
before diving into the logic of the operator code itself.
Hence, while limited, it is still a useful exercise.

To build complexer operators, we part away from BASH and look into writing Operators with Go and Python.
Both languages support asynchronous programming, which makes working with events
easy. 
We will not, however, directly use events. We leave this grunt work to the developers
of Operator Framework and build upon their work. This allows us to
concentrate on the logic of our bussiness needs, rather than the low level work with streams of
events.

.. raw:: html

   <div style = "display:block; clear:both; page-break-after:always;"></div>
