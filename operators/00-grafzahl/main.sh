#!/bin/bash

###
# Grafzahl likes counting things, and when there are more things to counts the happier
# he is.
# This silly operator just counts Deployments and Pods in the cluster.
###
NUM_PODS=0
NUM_DEPLOYMENTS=0

while true
do
	DEPLOYMENTS=$(kubectl get deployments.apps -A -o json | jq '.items | length')
	PODS=$(kubectl get pods -A -o json | jq '.items | length')
	echo Deployments now $DEPLOYMENTS, previously $NUM_DEPLOYMENTS
	echo Pods now $PODS, previously  $NUM_PODS
	if [[ $PODS -gt $NUM_PODS ]]; then
		echo "Pods number increased! 🧛 is 😄!"
	elif [[ $PODS -eq $NUM_PODS ]]; then
		echo "Pods number is the same! 🧛 is 😐!"
	elif [[  $PODS -lt $NUM_PODS ]]; then
		echo "Pods number is the smaller! 🧛 is 😔"
	fi
	if [[ $DEPLOYMENTS -gt $NUM_DEPLOYMENTS ]]; then
		echo "Deployments number increased! 🧛 is 😄!"
	elif [[ $DEPLOYMENTS -eq $NUM_DEPLOYMENTS ]]; then
		echo "Deployments number is the same! 🧛 is 😐!"
	elif [[  $DEPLOYMENTS -lt $NUM_DEPLOYMENTS ]]; then
		echo "Deployments number is the smaller! 🧛 is !😔"
	fi
	NUM_PODS=$PODS
	NUM_DEPLOYMENTS=$DEPLOYMENTS

	sleep 10
done
