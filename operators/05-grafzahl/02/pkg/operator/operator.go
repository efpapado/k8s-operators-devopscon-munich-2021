package operator

import (
	"log"
	"sync"
	"time"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var (
	VERSION      = "0.0.0.dev"
	resyncPeriod = 10 * time.Second
)

type Options struct {
	KubeConfig string
}

type Grafzahl struct {
	Options
	clientset *kubernetes.Clientset
}

func New(options Options) *Grafzahl {
	config := newClientConfig(options)

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatalf("Couldn't create Kubernetes client: %s", err)
	}

	gz := &Grafzahl{
		Options:   options,
		clientset: clientset,
	}

	return gz
}

func (example *Grafzahl) Run(stopCh <-chan struct{}, wg *sync.WaitGroup) {
	log.Printf("Grafzahl %v is ready to count \n", VERSION)
}

func newClientConfig(options Options) *rest.Config {
	rules := clientcmd.NewDefaultClientConfigLoadingRules()
	overrides := &clientcmd.ConfigOverrides{}

	if options.KubeConfig != "" {
		rules.ExplicitPath = options.KubeConfig
	}

	config, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(rules, overrides).ClientConfig()
	if err != nil {
		log.Fatalf("Couldn't get Kubernetes default config: %s", err)
	}

	return config
}
