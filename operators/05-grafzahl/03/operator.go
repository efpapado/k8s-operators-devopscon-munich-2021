package grafzahl

import (
	"context"
	"fmt"
	"log"
	"time"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"

	"github.com/oz123/grafzahl/internal"
)

const defaultResyncInterval = time.Second * 30

type Grafzahl struct {
	clientset         *kubernetes.Clientset
	podwatcher        *podWatcher
	deploymentwatcher *deploymentWatcher
}

func New(kubeConfigPath string) (*Grafzahl, error) {
	config, err := newClientConfig(kubeConfigPath)
	if err != nil {
		return nil, fmt.Errorf("config: %w", err)
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("clientset: %w", err)
	}

	gz := &Grafzahl{
		clientset:         clientset,
		podwatcher:        newPodWatcher(clientset),
		deploymentwatcher: newDeploymentWatcher(clientset),
	}

	return gz, nil
}

func (gz *Grafzahl) Run(ctx context.Context) error {
	log.Printf("Grafzahl %v is ready to count \n", internal.Version)

	stopChan := make(chan struct{})

	go gz.podwatcher.run(stopChan)
	go gz.deploymentwatcher.run(stopChan)

	select {
	case <-ctx.Done():
		close(stopChan)
		break
	}

	return nil
}

func newClientConfig(kubeConfigPath string) (*rest.Config, error) {
	defaultRules := clientcmd.NewDefaultClientConfigLoadingRules()

	if kubeConfigPath != "" {
		defaultRules.ExplicitPath = kubeConfigPath
	}

	config, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(defaultRules, nil).ClientConfig()
	if err != nil {
		return nil, fmt.Errorf("creating default config failed: %w", err)
	}

	return config, nil
}
