package operator

import (
	"fmt"
	"log"
	"sync"
	"time"

	corev1 "k8s.io/api/core/v1"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
)

type PodWatcher struct {
	podInformer cache.SharedIndexInformer
}

func newPodWatacher(client *kubernetes.Clientset) *PodWatcher {
	pw := &PodWatcher{}
	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(client, time.Second*30)

	podInformer := kubeInformerFactory.Core().V1().Pods().Informer()

	podInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			pod := obj.(*corev1.Pod)
			fmt.Printf("pod added: %s/%s\n", pod.Namespace, pod.Name)
		},
		DeleteFunc: func(obj interface{}) {
			fmt.Printf("pod deleted: %s \n", obj)
		},
		UpdateFunc: func(oldObj, newObj interface{}) {
			fmt.Printf("pod updated: %s \n", newObj)
		},
	})

	pw.podInformer = podInformer
	return pw
}

func (p *PodWatcher) Run(stopCh <-chan struct{}, wg *sync.WaitGroup) {
	defer wg.Done()
	wg.Add(1)

	log.Printf("Start watching pods")

	go p.podInformer.Run(stopCh)

	<-stopCh
}
