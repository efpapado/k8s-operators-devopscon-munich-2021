package operator

import (
	"fmt"
	"log"
	"sync"
	"time"

	appsv1 "k8s.io/api/apps/v1"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
)

type DeploymentWatcher struct {
	deploymentInformer cache.SharedIndexInformer
}

func onAdddeployment(obj interface{}) {
	d := obj.(*appsv1.Deployment)
	fmt.Printf("deployment added: %s/%s\n", d.Namespace, d.Name)
}

func newDeploymentWatcher(client *kubernetes.Clientset) *DeploymentWatcher {
	dw := &DeploymentWatcher{}
	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(client, time.Second*30)

	deploymentInformer := kubeInformerFactory.Apps().V1().Deployments().Informer()

	deploymentInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: onAdddeployment,
		DeleteFunc: func(obj interface{}) {
			fmt.Printf("deployment deleted: %s \n", obj)
		},
		UpdateFunc: func(oldObj, newObj interface{}) {
			fmt.Printf("deployment updated: %s \n", newObj)
		},
	})

	dw.deploymentInformer = deploymentInformer
	return dw
}

func (d *DeploymentWatcher) Run(stopCh <-chan struct{}, wg *sync.WaitGroup) {
	defer wg.Done()
	wg.Add(1)

	log.Printf("Start watching deployments")

	go d.deploymentInformer.Run(stopCh)

	<-stopCh
}
